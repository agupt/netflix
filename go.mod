module gitlab.com/agupt/netflix

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/hashicorp/golang-lru v0.5.4
	github.com/stretchr/testify v1.5.1
)
