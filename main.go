package main

import (
	"gitlab.com/agupt/netflix/server"
	"log"
	"net/http"
	"time"
)

func main() {
	h, err := server.NewDefaultHandler()
	if err != nil {
		log.Fatal(err)
	}
	srv := &http.Server{
		Handler: h,
		Addr:    "127.0.0.1:8080",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Println("User movie detail service running at http://localhost:8080/")
	log.Fatal(srv.ListenAndServe())
}
