// +build integration

package main

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	http2 "gitlab.com/agupt/netflix/http"
	"gitlab.com/agupt/netflix/server"
	"gitlab.com/agupt/netflix/service"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGettable_Get(t *testing.T) {
	client := &http.Client{}
	g := http2.NewGettable(client)
	assert.NotNil(t, g)
	ok := func(url string) {
		res, err := g.Get(context.Background(), url)
		assert.NoError(t, err)
		assert.Equal(t, http.StatusOK, res.StatusCode)
		data, err := ioutil.ReadAll(res.Body)
		assert.NoError(t, err)
		assert.NotNil(t, data)
	}
	ok("http://localhost:7003/793")
	ok("http://localhost:7002/793")
	ok("http://localhost:7001/users/12345")
}

func TestNewArtService(t *testing.T) {
	art, err := service.NewArtService(service.ArtServiceBaseURL)
	assert.NoError(t, err)
	req := service.MovieArtRequest{ID: 793}
	res, err := art.Art(context.Background(), req)
	assert.NoError(t, err)
	assert.NotEmpty(t, res)

	var reqs []service.MovieArtRequest
	for i := 0; i < service.DefaultMaxConcurrentRequestPerBatch*2; i++ {
		reqs = append(reqs, req)
	}
	resChan := art.Arts(context.Background(), reqs)
	var batchResponse []service.BatchMovieArtResponse
	for br := range resChan {
		batchResponse = append(batchResponse, br)
	}
	assert.Len(t, batchResponse, len(reqs))
	for _, br := range batchResponse {
		assert.NoError(t, br.Err)
		assert.NotEmpty(t, br.Res)
	}
}

func TestNewTitleService(t *testing.T) {
	title, err := service.NewTitleService(service.TitleServiceBaseURL)
	assert.NoError(t, err)
	req := service.MovieTitleRequest{ID: 793}
	res, err := title.Title(context.Background(), req)
	assert.NoError(t, err)
	assert.NotEmpty(t, res)

	var reqs []service.MovieTitleRequest
	for i := 0; i < service.DefaultMaxConcurrentRequestPerBatch*2; i++ {
		reqs = append(reqs, req)
	}
	resChan := title.Titles(context.Background(), reqs)
	var batchResponse []service.BatchMovieTitleResponse
	for br := range resChan {
		batchResponse = append(batchResponse, br)
	}
	assert.Len(t, batchResponse, len(reqs))
	for _, br := range batchResponse {
		assert.NoError(t, br.Err)
		assert.NotEmpty(t, br.Res)
	}
}

func TestNewUserMovieListService(t *testing.T) {
	movielist, err := service.NewMovieListService(service.MovieListServiceBaseURL)
	assert.NoError(t, err)
	req := service.MovieListRequest{ID: 12345}
	res, err := movielist.MovieIds(context.Background(), req)
	assert.NoError(t, err)
	assert.NotEmpty(t, res)
}

func TestNewMovieDetailService(t *testing.T) {
	art, err := service.NewArtService(service.ArtServiceBaseURL)
	assert.NoError(t, err)
	title, err := service.NewTitleService(service.TitleServiceBaseURL)
	assert.NoError(t, err)
	list, err := service.NewMovieListService(service.MovieListServiceBaseURL)
	assert.NoError(t, err)
	srv := service.NewMovieDetailService(list, art, title)
	res, err := srv.MovieDetails(context.Background(), service.UserMovieDetailsRequest{ID: service.UserID(12345)})
	assert.NoError(t, err)
	assert.NotNil(t, res)
	assert.True(t, len(res) > 0)
	buf := new(bytes.Buffer)
	enc := json.NewEncoder(buf)
	assert.NoError(t, enc.Encode(res))
	var exp service.UserMovieDetailsResponse
	assert.NoError(t, json.NewDecoder(buf).Decode(&exp))
	assert.Equal(t, res, exp)
}

func TestHandler(t *testing.T) {
	h, err := server.NewDefaultHandler()
	assert.NoError(t, err)
	r := httptest.NewRequest(http.MethodGet, "http://:/users/12345/movies", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)
	assert.Equal(t, http.StatusOK, w.Code)
	var got service.UserMovieDetailsResponse
	body := w.Body.String()
	w.Body.WriteString(body)
	assert.NoError(t, json.NewDecoder(w.Body).Decode(&got), body)
}

func BenchmarkHandler(b *testing.B) {
	h, err := server.NewDefaultHandler()
	assert.NoError(b, err)
	r := httptest.NewRequest(http.MethodGet, "http://:/users/12345/movies", nil)
	for i := 0; i < b.N; i++ {
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)
		assert.Equal(b, http.StatusOK, w.Code)
	}
}
