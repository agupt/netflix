# Netflix Take Home

This program implements a server to get details on movies
related to a user. 

## Dependency

- Movie List, Movie Title and Movie Art service running. 
- If you plan to build / test 

   - Install GO version `>=go1.14.2`, for installation instruction visit [https://golang.org/](https://golang.org/)
   - Download Dependency, clone this repo and run `go mod download` in code directory.  

**all external dependcies are listed in [go.mod](go.mod) file**
## Build/Test
Running following command in the directory containing code. 
- Build `go build ./...`
- Test `go test -coverprofile=out ./...`
- Integration Test required jar running `go test -tags=integration -coverprofile=out ./...`
- HTML Coverage Report `go tool cover -html=out`

## Running Service

`go run main.go` should start service

A cross compiled binary is provide choose one executable from following option. 

**NOTE: I wasn't able to test linux and darwin binaries as I was working on my personal windows, let me know if you see any issues.**

| ARCH      | FILE |
|-----------|------|
|windows|[movies-windows-amd64.exe](movies-windows-amd64.exe)|
|darwin|[movies-darwin-amd64](movies-darwin-amd64)|
|linux|[movies-linux-amd64](movies-linux-amd64)|

**A successful execution of executable should print following line in log**
```text
 User movie detail service running at http://localhost:8080/
```

## Reading Code
Code is divided into three sub modules

### http
`http` package under _http_ directory define interfaces for

- `Queryable` interface to get Resource URL from request payload.
- `Gettable` interface to synchronously _HTTP GET_ a resource.
   
   - `NewCacheGettable` returns a specialized `Gettable` that caches query response in an LRU cache. Size and Expiry time of keys is configurable.  
- `BatchGettable` interface to asynchronously and concurrently run many _HTTP GET_ calls. This interface is useful
to run operations such as get _title/art_ of multiple movies concurrently.
### service

`service` package under _service_ directory define HTTP request and response message format for four services. Namely, MovieList, MovieTitle, MovieArt and User Movie Details. 
GO struct in this package codify JSON responses. 

- All interaction with any of four service should be done via concrete service interface defined in this package. 
- Concrete service implementation encapsulate/delegate request to `Gettable` & `BatchGettable` interface which perform HTTP GET operations.  

### server

`server` package defines handler for User Movie detail service. This handler uses a concrete implementation of `UserMovieDetailsService` to get details on movies associated to users. 
This package also defines valid route path at which this handler listens. 

### main.go 

`main.go` instantiates a server using handler defined in package `server` and waits until user terminates server.  

## Design Consideration

#### Discussion of major design decisions

Api exposed by this service performs _Scatter_ and _Gather_ operations, design of server is such that:

- Responses can be cached for better performance.
- Batch operations can be performed asynchronously in separate GOROUTINES (very lightweight threads)
- Fanout of batch operations can be controlled to avoid explosion in # of threads.
- All threads running fanout are controlled by single time bounded context which can be used to configure a **time bound** on fanout operation. 
- Response of async batch operations are available to caller as soon as individual items in batch finish processing, for example as soon as title of a movie id is fetched it is made available to caller on a shared channel. It is possible that during this time batch is not fully processed. 
- If caller fails to read response from batch processing any remaing concurrent processing is either paused or cancelled if time out exceeds. 

#### Discussion of your solution’s performance
- Cache used to store ART and TITLE of movies make responses in certain cases almost instaneous. 
- It was hard to measure exact performance of service because response of movie title, art and list service were not stable i.e. response for same user and movie id will change over time. 
- I did not do p50, p95 or p99 latency measurements. 

#### How long did your solution take you? What scope did you cut? What would you have done if you had more time?
- Solution tooks me 1 hr of working for 5 days. 
- I cut following from my inital scope
   - performance analysis on latency using [https://github.com/codesenberg/bombardier](https://github.com/codesenberg/bombardier)
   - 100% test coverage
   - Logging support
   - metric support 
#### How did you handle the latency and error rates from each of the services?

- Latency from upstream services is handled via.
   
   - LRU Cache to cache responses
   - TimeBound execution on Batch HTTP GET calls. 
   - Bounded concurrent processing of batch calls
   - Controlled concurrent fan out on batch calls to avoid overloading. 
   - Async response availability via in process channels to concurrently do data processing and response building.

- Error Rates

   - User Movie Details service may return partial response if some calls to art and title service fail. In such case a http status code of **206** is returned. 
   - Only if calls to movie list or all calls to fetch title & art fail a 500 is returned. 
     

#### What would you do to make this service production ready?

I would do many things few of them are: 

- Benchmark performance.
- Support for log
- support for metrics 
- Containerization 
- 100% test coverage and integration test. 
- Access control to service. 
- SLA / SLO definition 
- RateLimiting 
- Standardize http route/path/query param definition withing Service interface. User external library to reduce code duplication. 

