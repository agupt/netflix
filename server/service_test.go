package server

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/agupt/netflix/service"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

type mockDetailsService struct {
	res    service.UserMovieDetailsResponse
	err    error
	userID int
	t      *testing.T
}

func (m mockDetailsService) MovieDetails(ctx context.Context, req service.UserMovieDetailsRequest) (service.UserMovieDetailsResponse, error) {
	assert.Equal(m.t, m.userID, int(req.ID))
	return m.res, m.err
}

func TestNewHandler(t *testing.T) {
	exp := service.UserMovieDetailsResponse([]service.UserMovieDetail{{
		Title: "title",
		Image: "image",
		MID:   service.MovieID(123),
	}})
	details := mockDetailsService{
		res:    exp,
		err:    nil,
		t:      t,
		userID: 1234,
	}
	h, err := NewHandler(details, time.Second)
	assert.NoError(t, err)
	r := httptest.NewRequest(http.MethodGet, "http://:/users/1234/movies", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)
	assert.Equal(t, http.StatusOK, w.Code)
	var got service.UserMovieDetailsResponse
	assert.NoError(t, json.NewDecoder(w.Body).Decode(&got))
	assert.Equal(t, got, exp)
}

func TestNewHandler_DetailsError(t *testing.T) {
	details := mockDetailsService{
		res:    nil,
		err:    fmt.Errorf("invalid request"),
		t:      t,
		userID: 1234,
	}
	h, err := NewHandler(details, time.Second)
	assert.NoError(t, err)
	r := httptest.NewRequest(http.MethodGet, "http://:/users/1234/movies", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
	assert.Equal(t, "failed to get movie details for user invalid request", w.Body.String())
}

func TestNewHandler_PartialContent(t *testing.T) {
	exp := service.UserMovieDetailsResponse([]service.UserMovieDetail{{
		Title: "title",
		Image: "image",
		MID:   service.MovieID(123),
	}})
	details := mockDetailsService{
		res:    exp,
		err:    fmt.Errorf("invalid request"),
		t:      t,
		userID: 1234,
	}
	h, err := NewHandler(details, time.Second)
	assert.NoError(t, err)
	r := httptest.NewRequest(http.MethodGet, "http://:/users/1234/movies", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)
	assert.Equal(t, http.StatusPartialContent, w.Code)
	var got service.UserMovieDetailsResponse
	assert.NoError(t, json.NewDecoder(w.Body).Decode(&got))
	assert.Equal(t, got, exp)
}

func TestNewHandler_BadUserID(t *testing.T) {
	details := mockDetailsService{
		res:    nil,
		err:    fmt.Errorf("invalid request"),
		t:      t,
		userID: 1234,
	}
	h, err := NewHandler(details, time.Second)
	assert.NoError(t, err)
	r := httptest.NewRequest(http.MethodGet, "http://:/users/foo/movies", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)
	assert.Equal(t, http.StatusBadRequest, w.Code)
	assert.Equal(t, "invalid user id", w.Body.String())
}
