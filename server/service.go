package server

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/agupt/netflix/service"
	"net/http"
	"strconv"
	"time"
)

const (
	userID = "user_id"
)
const DefaultMaxProcessingTime = time.Second * 20

func getUserID(r *http.Request) (service.UserID, error) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars[userID])
	if err != nil {
		return 0, err
	}
	return service.UserID(id), nil
}

type srv struct {
	details           service.UserMovieDetailsService
	maxProcessingTime time.Duration
}

func NewHandler(details service.UserMovieDetailsService, timeout time.Duration) (http.Handler, error) {
	s := &srv{
		details:           details,
		maxProcessingTime: timeout,
	}
	m := mux.NewRouter()
	m.HandleFunc("/users/{user_id}/movies", s.userDetailServiceHandler).Methods(http.MethodGet).Schemes("http")
	return m, nil
}

func NewDefaultHandler() (http.Handler, error) {
	details, err := service.DefaultMovieDetailsService()
	if err != nil {
		return nil, err
	}
	return NewHandler(details, DefaultMaxProcessingTime)
}

func (s *srv) userDetailServiceHandler(w http.ResponseWriter, r *http.Request) {
	id, err := getUserID(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid user id"))
		return
	}
	req := service.UserMovieDetailsRequest{ID: id}
	ctx, cancel := context.WithTimeout(context.Background(), s.maxProcessingTime)
	defer cancel()
	res, err := s.details.MovieDetails(ctx, req)
	if err != nil && len(res) == 0 {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("failed to get movie details for user %v", err)))
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusPartialContent)
	} else {
		w.WriteHeader(http.StatusOK)
	}
	if err := json.NewEncoder(w).Encode(res); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("failed to build json response %v", err)))
		return
	}
}
