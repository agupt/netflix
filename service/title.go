package service

import (
	"context"
	h "gitlab.com/agupt/netflix/http"
	"net/http"
)

type titleService struct {
	g    h.Gettable
	bg   h.BatchGettable
	base string
}

func (t *titleService) Title(ctx context.Context, req MovieTitleRequest) (MovieTitleResponse, error) {
	u := req.Query(t.base)
	res, err := t.g.Get(ctx, u)
	if err != nil {
		return "", err
	}
	data, err := h.GetDataFromResponse(res)
	return MovieTitleResponse(data), err
}

func (t *titleService) Titles(ctx context.Context, reqs []MovieTitleRequest) <-chan BatchMovieTitleResponse {
	urlString := make([]string, 0, len(reqs))
	urlToReqMap := make(map[string]MovieTitleRequest)
	for _, req := range reqs {
		url := req.Query(t.base)
		urlString = append(urlString, url)
		urlToReqMap[url] = req
	}
	if len(urlString) == 0 {
		return nil
	}
	rawResChan := t.bg.Gets(ctx, urlString)
	resChan := make(chan BatchMovieTitleResponse, len(urlString))
	go func() {
		for res := range rawResChan {
			if res.Err != nil {
				resChan <- BatchMovieTitleResponse{
					Res: "",
					Err: res.Err,
					Req: urlToReqMap[res.ReqUrl],
				}
			} else {
				data, err := h.GetDataFromResponse(res.Res)
				resChan <- BatchMovieTitleResponse{Res: MovieTitleResponse(data), Err: err, Req: urlToReqMap[res.ReqUrl]}
			}
		}
		close(resChan)
	}()
	return resChan
}

func NewTitleService(base string) (MovieTitleService, error) {
	client := &http.Client{}
	g, err := h.NewCacheGettable(h.NewGettable(client), DefaultCacheSize, DefaultCacheExpirationTime)
	if err != nil {
		return nil, err
	}
	bg := h.NewBatchGettable(g, DefaultMaxConcurrentRequestPerBatch)
	return &titleService{
		base: base,
		g:    g,
		bg:   bg,
	}, nil
}
