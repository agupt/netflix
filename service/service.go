package service

import (
	"context"
	"fmt"
	"path"
	"time"
)

const (
	DefaultCacheSize                    = 128
	DefaultCacheExpirationTime          = time.Second * 5
	DefaultMaxConcurrentRequestPerBatch = 50
)

const (
	ArtServiceBaseURL       = "http://localhost:7003"
	TitleServiceBaseURL     = "http://localhost:7002"
	MovieListServiceBaseURL = "http://localhost:7001"
)

// MovieID represent id of a movie
type MovieID int

// UserID represent id of a user
type UserID int

// UserMovieDetail represent details of movie associated to a user
type UserMovieDetail struct {
	Title MovieTitleResponse `json:"title"`
	Image MovieArtResponse   `json:"image"`
	MID   MovieID            `json:"movieId"`
}

// UserMovieDetailsResponse represent details of many movies associated to a user.
type UserMovieDetailsResponse []UserMovieDetail

// UserMovieDetailsRequest represent request to get details on movie associated with a user.
type UserMovieDetailsRequest struct {
	ID UserID `json:"id"`
}

func (u UserMovieDetailsRequest) Query(base string) string {
	p := path.Join(base, "users", fmt.Sprintf("%d", u.ID), "movies")
	return p
}

// UserMovieDetailsService is a service to get movies & details of movies associated to user.
type UserMovieDetailsService interface {
	// MovieDetails returns details of movies associated with user.
	MovieDetails(ctx context.Context, req UserMovieDetailsRequest) (UserMovieDetailsResponse, error)
}

// MovieListResponse represent response of service which returns MovieIDs associated to a user.
type MovieListResponse []MovieID

// MovieListRequest represent request to get ID of movies associated to a user.
type MovieListRequest struct {
	ID UserID
}

func (m MovieListRequest) Query(base string) string {
	p := fmt.Sprintf("%s/users/%d", base, m.ID)
	return p
}

// UserMovieListService is a service to get list of Movies associated to a user.
type UserMovieListService interface {
	// MovieIds returns movie ids associated with user
	MovieIds(ctx context.Context, req MovieListRequest) (MovieListResponse, error)
}

// MovieTitleResponse represent title of a movie
type MovieTitleResponse string

// MovieTitleRequest represent request to get title associated with Movie ID.
type MovieTitleRequest struct {
	ID MovieID `json:"id"`
}

func (m MovieTitleRequest) Query(base string) string {
	p := fmt.Sprintf("%s/%d", base, m.ID)
	return p
}

// BatchMoveTitleResponse represent a response of batch request to get title of many movies.
type BatchMovieTitleResponse struct {
	Res MovieTitleResponse
	Err error
	Req MovieTitleRequest
}

// MovieTitleService is a service to get Titles associated with to movie ID.
type MovieTitleService interface {
	// Title returns title of movie.
	Title(ctx context.Context, req MovieTitleRequest) (MovieTitleResponse, error)
	// Titles performs a asynchronous request to get Titles associated with many movies it returns a channel to read
	// response associated with each movie title request.
	Titles(ctx context.Context, reqs []MovieTitleRequest) <-chan BatchMovieTitleResponse
}

// MovieArtResponse represent location of movie art.
type MovieArtResponse string

// MovieArtRequest represent request to get MovieArt associated with movie ID.
type MovieArtRequest struct {
	ID MovieID
}

func (m MovieArtRequest) Query(base string) string {
	p := fmt.Sprintf("%s/%d", base, m.ID)
	return p
}

// BatchMovieArtResponse represent response of batch request to get art of many movie.
type BatchMovieArtResponse struct {
	Res MovieArtResponse
	Err error
	Req MovieArtRequest
}

// MovieArtService is a service to get Art associated with a movie.
type MovieArtService interface {
	// Art returns location of movie art.
	Art(ctx context.Context, req MovieArtRequest) (MovieArtResponse, error)
	// Arts performs a asynchronous request to get Arts associated with many movies it returns a channel to read
	// response associated with each movie art request.
	Arts(ctx context.Context, reqs []MovieArtRequest) <-chan BatchMovieArtResponse
}
