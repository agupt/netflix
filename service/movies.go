package service

import (
	"context"
	"encoding/json"
	"fmt"
	h "gitlab.com/agupt/netflix/http"
	"net/http"
)

type movieListService struct {
	g    h.Gettable
	base string
}

func NewMovieListService(base string) (UserMovieListService, error) {
	client := &http.Client{}
	g, err := h.NewCacheGettable(h.NewGettable(client), DefaultCacheSize, DefaultCacheExpirationTime)
	if err != nil {
		return nil, err
	}
	return &movieListService{
		base: base,
		g:    g,
	}, nil
}

func (m *movieListService) MovieIds(ctx context.Context, req MovieListRequest) (MovieListResponse, error) {
	u := req.Query(m.base)
	res, err := m.g.Get(ctx, u)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		_, err := h.GetDataFromResponse(res)
		return nil, err
	}
	var movieIds MovieListResponse
	if err := json.NewDecoder(res.Body).Decode(&movieIds); err != nil {
		return nil, err
	}
	return movieIds, nil
}

type movieDetailService struct {
	list  UserMovieListService
	art   MovieArtService
	title MovieTitleService
}

func NewMovieDetailService(list UserMovieListService, art MovieArtService, title MovieTitleService) UserMovieDetailsService {
	return &movieDetailService{
		list:  list,
		art:   art,
		title: title,
	}
}

func (m *movieDetailService) MovieDetails(ctx context.Context, req UserMovieDetailsRequest) (UserMovieDetailsResponse, error) {
	listReq := MovieListRequest{ID: req.ID}
	movieIds, err := m.list.MovieIds(ctx, listReq)
	if err != nil {
		return nil, err
	}
	if len(movieIds) == 0 {
		return nil, fmt.Errorf("no movies found for user:%v", req.ID)
	}
	// make title and art request
	titleReqs := make([]MovieTitleRequest, len(movieIds))
	artReqs := make([]MovieArtRequest, len(movieIds))
	for i, id := range movieIds {
		index := i
		movieId := id
		titleReqs[index] = MovieTitleRequest{ID: movieId}
		artReqs[index] = MovieArtRequest{ID: movieId}
	}
	titleChan := m.title.Titles(ctx, titleReqs)
	artChan := m.art.Arts(ctx, artReqs)

	// process async response from art and title services
	idToMovieDetailsMap := make(map[MovieID]*UserMovieDetail)
	initDetails := func(id MovieID) *UserMovieDetail {
		if _, ok := idToMovieDetailsMap[id]; !ok {
			idToMovieDetailsMap[id] = &UserMovieDetail{
				Title: "",
				Image: "",
				MID:   id,
			}
		}
		return idToMovieDetailsMap[id]
	}
	failed := make(map[MovieID]struct{})
	titleOk, artOk := true, true
	for titleOk || artOk {
		select {
		case title, ok := <-titleChan:
			titleOk = ok
			if ok {
				if title.Err != nil {
					failed[title.Req.ID] = struct{}{}
				} else {
					initDetails(title.Req.ID).Title = title.Res
				}
			}
		case art, ok := <-artChan:
			artOk = ok
			if ok {
				if art.Err != nil {
					failed[art.Req.ID] = struct{}{}
				} else {
					initDetails(art.Req.ID).Image = art.Res
				}
			}
		}
	}
	if len(failed) == len(idToMovieDetailsMap) {
		return nil, fmt.Errorf("failed to get art and title for all movies")
	}
	// filter out movies that failed generate response
	var res UserMovieDetailsResponse
	var failedIds []string
	for id := range idToMovieDetailsMap {
		if _, ok := failed[id]; !ok {
			res = append(res, *idToMovieDetailsMap[id])
		} else {
			failedIds = append(failedIds, fmt.Sprintf("%v", id))
		}
	}
	if len(failedIds) != 0 {
		err = fmt.Errorf("failed to get art or title for movies ids: %v", failedIds)
	}
	return res, err
}

func DefaultMovieDetailsService() (UserMovieDetailsService, error) {
	art, err := NewArtService(ArtServiceBaseURL)
	if err != nil {
		return nil, err
	}
	title, err := NewTitleService(TitleServiceBaseURL)
	if err != nil {
		return nil, err
	}
	list, err := NewMovieListService(MovieListServiceBaseURL)
	if err != nil {
		return nil, err
	}
	return NewMovieDetailService(list, art, title), nil
}
