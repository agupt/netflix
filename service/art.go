package service

import (
	"context"
	h "gitlab.com/agupt/netflix/http"
	"net/http"
)

type artService struct {
	base string
	g h.Gettable
	bg h.BatchGettable
}

func NewArtService(base string) (MovieArtService, error) {
	client := &http.Client{}
	g, err := h.NewCacheGettable(h.NewGettable(client),DefaultCacheSize, DefaultCacheExpirationTime)
	if err != nil {
		return nil, err
	}
	bg := h.NewBatchGettable(g, DefaultMaxConcurrentRequestPerBatch)
	return &artService{
		base: base,
		g:    g,
		bg:   bg,
	}, nil
}


func (a *artService) Art(ctx context.Context, req MovieArtRequest) (MovieArtResponse, error) {
	u := req.Query(a.base)
	res, err := a.g.Get(ctx, u)
	if err != nil {
		return "", err
	}
	data, err := h.GetDataFromResponse(res)
	return MovieArtResponse(data), err
}

func (a *artService) Arts(ctx context.Context, reqs []MovieArtRequest) <-chan BatchMovieArtResponse {
	urlString :=  make([]string, 0, len(reqs))
	urlToReqMap := make(map[string]MovieArtRequest)
	for _, req := range reqs {
		u := req.Query(a.base)
		urlString = append(urlString, u)
		urlToReqMap[u] = req
	}
	if len(urlString) == 0 {
		return nil
	}
	rawResChan := a.bg.Gets(ctx, urlString)
	resChan := make(chan BatchMovieArtResponse, len(urlString))
	go func() {
		for res := range rawResChan {
			if res.Err != nil {
				resChan <- BatchMovieArtResponse{
					Res: "",
					Err: res.Err,
					Req: urlToReqMap[res.ReqUrl],
				}
			} else {
				data, err := h.GetDataFromResponse(res.Res)
				resChan <-BatchMovieArtResponse{Res: MovieArtResponse(data), Err:err, Req: urlToReqMap[res.ReqUrl]}
			}
		}
		close(resChan)
	}()
	return resChan
}



