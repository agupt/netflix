package service

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMovieArtRequest_Query(t *testing.T) {
	r := MovieArtRequest{ID: 123}
	u := r.Query("http://localhost:793")
	assert.Equal(t, "http://localhost:793/123", u)
}

func TestMovieTitleRequest_Query(t *testing.T) {
	r := MovieTitleRequest{ID: 123}
	u := r.Query("http://localhost:793")
	assert.Equal(t, "http://localhost:793/123", u)
}

func TestMovieListRequest_Query(t *testing.T) {
	r := MovieListRequest{ID: 123}
	u := r.Query("http://localhost:793")
	assert.Equal(t, "http://localhost:793/users/123", u)
}

type mockMovieListService struct {
	ids []int
	err error
}

func (m mockMovieListService) MovieIds(ctx context.Context, req MovieListRequest) (MovieListResponse, error) {
	var res MovieListResponse
	for _, id := range m.ids {
		res = append(res, MovieID(id))
	}
	return res, m.err
}

type mockMovieArtTitleService struct {
	ids            []int
	failedArtIds   []int
	failedTitleIds []int
}

func (m mockMovieArtTitleService) Art(ctx context.Context, req MovieArtRequest) (MovieArtResponse, error) {
	panic("implement me")
}

func (m mockMovieArtTitleService) Arts(ctx context.Context, reqs []MovieArtRequest) <-chan BatchMovieArtResponse {
	resChan := make(chan BatchMovieArtResponse)
	go func() {
		for _, id := range m.ids {
			resChan <- BatchMovieArtResponse{
				Res: MovieArtResponse(fmt.Sprintf("movie-art-%d", id)),
				Err: nil,
				Req: MovieArtRequest{ID: MovieID(id)},
			}
		}
		for _, id := range m.failedTitleIds {
			resChan <- BatchMovieArtResponse{
				Res: "",
				Err: fmt.Errorf("faile to get art for movie %d", id),
				Req: MovieArtRequest{ID: MovieID(id)},
			}
		}
		close(resChan)
	}()
	return resChan
}

func (m mockMovieArtTitleService) Title(ctx context.Context, req MovieTitleRequest) (MovieTitleResponse, error) {
	panic("implement me")
}

func (m mockMovieArtTitleService) Titles(ctx context.Context, reqs []MovieTitleRequest) <-chan BatchMovieTitleResponse {
	resChan := make(chan BatchMovieTitleResponse)
	go func() {
		for _, id := range m.ids {
			resChan <- BatchMovieTitleResponse{
				Res: MovieTitleResponse(fmt.Sprintf("movie-title-%d", id)),
				Err: nil,
				Req: MovieTitleRequest{ID: MovieID(id)},
			}
		}
		for _, id := range m.failedArtIds {
			resChan <- BatchMovieTitleResponse{
				Res: "",
				Err: fmt.Errorf("failed to get title for movie id %d", id),
				Req: MovieTitleRequest{ID: MovieID(id)},
			}
		}
		close(resChan)
	}()
	return resChan
}

func TestNewMovieDetailService(t *testing.T) {
	movieIds := []int{1, 2, 3, 4, 5, 6}
	list := mockMovieListService{
		ids: movieIds,
		err: nil,
	}
	art := mockMovieArtTitleService{
		ids:            movieIds,
		failedArtIds:   []int{5},
		failedTitleIds: []int{5, 6},
	}
	validMovieId := []int{1, 2, 3, 4}
	srv := NewMovieDetailService(list, art, art)
	res, err := srv.MovieDetails(context.Background(), UserMovieDetailsRequest{ID: UserID(12345)})
	assert.EqualError(t, err, "failed to get art or title for movies ids: [5 6]")
	assert.NotNil(t, res)
	assert.Len(t, res, len(validMovieId))
	resMap := make(map[int]UserMovieDetail)
	for _, r := range res {
		resMap[int(r.MID)] = r
	}
	for _, id := range validMovieId {
		assert.Contains(t, resMap, id)
		assert.Equal(t, fmt.Sprintf("movie-art-%d", id), string(resMap[id].Image))
		assert.Equal(t, fmt.Sprintf("movie-title-%d", id), string(resMap[id].Title))
	}
	for _, id := range art.failedArtIds {
		assert.NotContains(t, resMap, id)
	}
	for _, id := range art.failedTitleIds {
		assert.NotContains(t, resMap, id)
	}
}
