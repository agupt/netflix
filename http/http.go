package http

import (
	"bytes"
	"context"
	"fmt"
	"github.com/hashicorp/golang-lru"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

// Queryable define method to get resource url.
type Queryable interface {
	Query(base string) string
}

// Gettable define behaviour to GET resource associated with URL
type Gettable interface {
	// Get fetches resource associated with url
	Get(ctx context.Context, url string) (resp *http.Response, err error)
}

type gettable struct {
	c *http.Client
}

func (g gettable) Get(ctx context.Context, url string) (*http.Response, error) {
	c := g.c
	if c == nil {
		c = http.DefaultClient
	}
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	return c.Do(req)
}

// NewGettable returns a Gettable that delegates request to http client.
func NewGettable(c *http.Client) Gettable {
	return gettable{c: c}
}

type mockGettable struct {
	urlResponses map[string]interface{}
}

func (m *mockGettable) Get(ctx context.Context, url string) (*http.Response, error) {
	if _, ok := m.urlResponses[url]; !ok {
		return nil, fmt.Errorf("invalid url %s response can't be mocked", url)
	}
	switch t := m.urlResponses[url].(type) {
	case *http.Response:
		return t, nil
	case error:
		return nil, t
	case time.Duration:
		wait := time.NewTimer(t)
		for {
			select {
			case <-ctx.Done():
				return nil, fmt.Errorf("mock request %s failed context expired", url)
			case <-wait.C:
				return nil, fmt.Errorf("mock request %s failed after sleeping for %v", url, t)
			}
		}
	default:
		return nil, fmt.Errorf("invalid mock response type: %T", t)
	}
}

// NewMockGettable returns a Gettable which mocks URl responses.
func NewMockGettable(urlResponses map[string]interface{}) Gettable {
	return &mockGettable{urlResponses: urlResponses}
}

type cacheEntry struct {
	data []byte
	t    time.Time
}

type cachedGettable struct {
	cache  *lru.Cache
	size   int
	g      Gettable
	expiry time.Duration
}

func (c *cachedGettable) Get(ctx context.Context, url string) (resp *http.Response, err error) {
	if val, ok := c.cache.Get(url); ok {
		entry := val.(cacheEntry)
		if entry.t.After(time.Now()) {
			return &http.Response{
				Status:        http.StatusText(http.StatusOK),
				StatusCode:    http.StatusOK,
				ContentLength: int64(len(entry.data)),
				Body:          ioutil.NopCloser(bytes.NewBuffer(entry.data)),
			}, nil
		}
	}
	res, err := c.g.Get(ctx, url)
	if err != nil {
		return res, err
	}
	data, err := GetDataFromResponse(res)
	// reset response body
	res.Body = ioutil.NopCloser(bytes.NewBuffer([]byte(data)))
	if err != nil {
		return res, err
	}
	entry := cacheEntry{
		data: []byte(data),
		t:    time.Now().Add(c.expiry),
	}
	c.cache.Add(url, entry)
	return res, err
}

// NewCacheGettable returns a Gettable which returns cached response.
func NewCacheGettable(delegate Gettable, size int, expiry time.Duration) (Gettable, error) {
	cache, err := lru.New(size)
	if err != nil {
		return nil, err
	}
	return &cachedGettable{
		cache:  cache,
		size:   size,
		g:      delegate,
		expiry: expiry,
	}, nil
}

type GettableResponse struct {
	Res    *http.Response
	Err    error
	ReqUrl string
}

// BatchGettable gets many response.
type BatchGettable interface {
	// Gets fetches resources associated with stream of URL and returns stream of response. It returns error if it fails to fetch all resources.
	Gets(ctx context.Context, urls []string) <-chan GettableResponse
}

type batchGettable struct {
	g         Gettable
	maxFanOut int
}

func (b *batchGettable) Gets(ctx context.Context, urls []string) <-chan GettableResponse {
	resChan := make(chan GettableResponse, len(urls))
	// max worker count
	fanOut := b.maxFanOut
	if fanOut > len(urls) {
		fanOut = len(urls)
	}
	// req channel with buffer size equal to count of url
	req := make(chan string, len(urls))
	var wg sync.WaitGroup
	getFunc := func() {
		defer wg.Done()
		for {
			select {
			case <-ctx.Done():
				return
			case url, open := <-req:
				if !open {
					return
				}
				res, err := b.g.Get(ctx, url)
				resChan <- GettableResponse{
					Res:    res,
					Err:    err,
					ReqUrl: url,
				}
			}
		}
	}
	// start worker to process urls
	for i := 0; i < fanOut; i++ {
		wg.Add(1)
		go getFunc()
	}
	// queue work for worker
	wg.Add(1)
	go func() {
		defer wg.Done()
		for _, u := range urls {
			req <- u
		}
		close(req)
	}()
	// wait for workers to finish executing and queue error for any unprocessed URL
	go func() {
		wg.Wait()
		for u := range req {
			// return an error for any un processed urls
			resChan <- GettableResponse{
				Res:    nil,
				Err:    fmt.Errorf("failed to process url: %s processing canceled", u),
				ReqUrl: u,
			}
		}
		close(resChan)
	}()
	return resChan
}

// NewBatchGettable returns a BatchGettable that delegates request to input Gettable.
func NewBatchGettable(g Gettable, maxFanOut int) BatchGettable {
	return &batchGettable{
		g:         g,
		maxFanOut: maxFanOut,
	}
}

func GetDataFromResponse(res *http.Response) (string, error) {
	if res == nil {
		return "", nil
	}
	var data []byte
	var err error
	if res.Body != nil {
		defer res.Body.Close()
		data, err = ioutil.ReadAll(res.Body)
	}
	if err != nil {
		return "", fmt.Errorf("query:%s code:%s Err:%v msg:%s", res.Request.URL.String(), res.Status, err, string(data))
	}
	if res.StatusCode != http.StatusOK {
		return "", fmt.Errorf("query:%s code:%s msg:%s", res.Request.URL.String(), res.Status, string(data))
	}
	return string(data), nil
}
