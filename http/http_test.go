package http

import (
	"bytes"
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"testing"
	"time"
)

func mockResponse(data string) *http.Response {
	return &http.Response{
		StatusCode: http.StatusOK,
		Body:       ioutil.NopCloser(bytes.NewBufferString(data)),
	}
}

func TestNewMockGettable(t *testing.T) {
	response := map[string]interface{}{"url": "valid"}
	g := NewMockGettable(response)
	assert.NotNil(t, g)
	assert.IsType(t, new(mockGettable), g)
	gc := g.(*mockGettable)
	assert.Equal(t, response, gc.urlResponses)
}

func TestMockGettable_Get(t *testing.T) {
	validRes := mockResponse("data")
	mockResponses := map[string]interface{}{"1": validRes, "2": fmt.Errorf("failed to request"), "3": time.Millisecond, "4": time.Second}
	g := NewMockGettable(mockResponses)
	res, err := g.Get(context.Background(), "1")
	assert.Nil(t, err)
	assert.Equal(t, validRes, res)
	data, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err)
	assert.Equal(t, "data", string(data))
	res, err = g.Get(context.Background(), "2")
	assert.Nil(t, res)
	assert.EqualError(t, err, "failed to request")
	res, err = g.Get(context.Background(), "3")
	assert.Nil(t, res)
	assert.EqualError(t, err, "mock request 3 failed after sleeping for 1ms")
	ctx, cancel := context.WithCancel(context.Background())
	cancel()
	res, err = g.Get(ctx, "4")
	assert.Nil(t, res)
	assert.EqualError(t, err, "mock request 4 failed context expired")
}

func TestNewBatchGettable(t *testing.T) {
	g := NewMockGettable(nil)
	bg := NewBatchGettable(g, 1)
	assert.NotNil(t, g)
	assert.IsType(t, new(batchGettable), bg)
	bgc := bg.(*batchGettable)
	assert.Equal(t, 1, bgc.maxFanOut)
	assert.Equal(t, g, bgc.g)
}

func TestBatchGettable_Gets(t *testing.T) {
	maxFanOut := 2
	mockResponses := map[string]interface{}{
		"1": mockResponse("1"),
		"2": mockResponse("2"),
	}
	g := NewMockGettable(mockResponses)
	bg := NewBatchGettable(g, maxFanOut)
	resChan := bg.Gets(context.Background(), []string{"1", "2"})
	resCount := 0
	for res := range resChan {
		assert.NoError(t, res.Err)
		if assert.NotNil(t, res.Res) {
			data, err := ioutil.ReadAll(res.Res.Body)
			assert.NoError(t, err)
			assert.Contains(t, mockResponses, string(data))
			assert.NotEmpty(t, res.ReqUrl)
		}
		resCount++
	}
	assert.Equal(t, len(mockResponses), resCount)
}

func TestBatchGettable_Gets_LongRunningGettable(t *testing.T) {
	maxFanOut := 1
	mockResponses := map[string]interface{}{
		"1": time.Second, // force gettable to wait for a sec to generate response
		"2": time.Second,
	}
	g := NewMockGettable(mockResponses)
	bg := NewBatchGettable(g, maxFanOut)
	ctx, cancel := context.WithCancel(context.Background())
	cancel() // cancel context to force early exit
	resChan := bg.Gets(ctx, []string{"1", "2"})
	resCount := 0
	for res := range resChan {
		assert.Nil(t, res.Res)
		assert.Error(t, res.Err)
		assert.NotEmpty(t, res.ReqUrl)
		resCount++
	}
	assert.Equal(t, len(mockResponses), resCount)
}

func TestCachedGettable_Get(t *testing.T) {
	mockRes := mockResponse("1")
	mockResponses := map[string]interface{}{
		"1": mockRes,
	}
	g := NewMockGettable(mockResponses)
	cg, err := NewCacheGettable(g, 2, time.Second)
	assert.IsType(t, new(cachedGettable), cg)
	ccg := cg.(*cachedGettable)
	assert.NoError(t, err)
	assert.NotNil(t, cg)
	res, err := cg.Get(context.Background(), "1")
	assert.NoError(t, err)
	assert.NotNil(t, res)
	data, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err)
	assert.Equal(t, "1", string(data))
	assert.True(t, ccg.cache.Contains("1"))
	res, err = cg.Get(context.Background(), "1")
	assert.NoError(t, err)
	assert.NotNil(t, res)
	data, err = ioutil.ReadAll(res.Body)
	assert.NoError(t, err)
	assert.Equal(t, "1", string(data))
}
